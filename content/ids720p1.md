+++
title = "CI With Github Actions"
date = 2023-09-12
weight = 1
+++
The purpose of this project is to build upon the last three mini-projects to simulate best practices of continuous integration in Data Science projects. I use the Congress dataset provided by FiveThirtyEight to produce sample descriptive statistics and visualizations.  
[Project Link](https://github.com/nogibjj/Jeremy_Tan_IDS706_Week3_Individual)
