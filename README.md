[![pipeline status](https://gitlab.com/jeremymtan/jeremytan_ids721_week1/badges/main/pipeline.svg)](https://gitlab.com/jeremymtan/jeremytan_ids721_week1/-/commits/main)

# Static Site with Zola

[portfolio webiste](https://jeremy-tan.vercel.app)

## Screenshot of Website 
![Screenshot_2024-01-30_at_4.01.44_PM](/uploads/ae5e13f73dd8c4690a853214ce7b988f/Screenshot_2024-01-30_at_4.01.44_PM.png)

## Purpose 
The purpose of this project is to create a static portfoilo website with Zola. I showcase my projects from IDS720, the pre-requiste of this course.

## Preparation
1. Install Zola 
2. Create website with `zola init`
3. Add a theme for css `cd themes` then `git submodule add <theme name>`
4. Create pages in the `content` folder 
5. `zola serve` to test website or `zola build`
6. Install Vercel CLI 
7. Login to Vercel with `vercel login`
8. Link your repo with `vercel link`
9. Copy your Vercel token, Vercel team id, and Vercel Org id to Gitlab secrets 
10. Push repo to run pipeline filr `.gitlab-ci.yml`

## References 
1. https://vercel.com/guides/how-can-i-use-gitlab-pipelines-with-vercel#configuring-gitlab-ci/cd-for-vercel
2. https://www.getzola.org/themes/hyde/
3. https://www.getzola.org/documentation/deployment/vercel/


1